//
//  SearchArticleViewController.swift
//  NYTApp
//
//  Created by Ankit on 9/6/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import UIKit

class SearchArticleViewController: BaseViewController {

    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var searchTetxField: UITextField!
    var searchText = ""
    let versionURL = "v2"
    var searchResultArray = [SearchList]()
    var overlayView = LoadingOverlay.shared

    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldFeature()
        // Do any additional setup after loading the view.
    }
    
    func textFieldFeature(){
          searchTetxField.setLeftPaddingPoints(10)
          searchButton.addShadow()
    }
    
    func fetchUserDataOfPage() {
        let urlString = "search/\(versionURL)/articlesearch.json?q=\(self.searchTetxField.text!)&page=\(0)&sort=newest&api-key=\(Keys.apiKey)"
        self.overlayView.showOverlay(view: self.view)
        DataManager.sharedInstance().SearchList(apiName: urlString,closure: { (result) in
            print(result)
            switch result {
            case .success(let dataList):
                print(dataList)
                self.overlayView.hideOverlayView()
                self.searchResultArray = dataList
                if dataList.count > 0{
                    let main = UIStoryboard(name: "Main", bundle: Bundle.main)
                    let searchArticleVC = main.instantiateViewController(withIdentifier: "articleListViewController") as! ArticleListViewController
                    searchArticleVC.searchListArray = self.searchResultArray;
                    self.navigationController!.pushViewController(searchArticleVC, animated:true)
                }else{
                    self.showAlert(title: "Article Not Found", message: "")
                }
                
                break
                
            case .failure(let error):
                print(error)
                self.overlayView.hideOverlayView()
                self.showAlert(title: "Alert", message: "Server Not Responding")
            }
            
        })
        
    }
    
    @IBAction func searchButtonAction(_ sender: Any) {
        if searchTetxField.text == ""{
            let alert = UIAlertController(title: "Enter text for Search Article", message: "", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else{
            if Connectivity.isConnectedToInternet {
                fetchUserDataOfPage()
            }
        }
    }
}

extension SearchArticleViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
//        doSearch()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            searchText = text.replacingCharacters(in: textRange,
                                                  with: string)
        }
        return true
    }
    
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
