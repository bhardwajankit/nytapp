//
//  NYTViewController.swift
//  NYTApp
//
//  Created by Ankit on 9/6/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import UIKit

class NYTViewController: BaseViewController {
    @IBOutlet weak var nytTableView: UITableView!
    var headerNames = ["SEARCH", "POPULAR"]
    var sectionNames = [["Search Articles"], ["viewed","shared","emailed"]]
    let versionURL = "v2"
    var articleListArray = [SearchList]()
    var overlayView = LoadingOverlay.shared

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBarItem()
        // Do any additional setup after loading the view.
    }
    
    func navigationBarItem(){
        self.navigationItem.setHidesBackButton(true, animated: false)
//        let attributes = [NSAttributedString.Key.font: UIFont(name: "Helvetica", size: 20)!]
//        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

    }
    
    func popularKeyData(popularKey: String) {
        let urlString = "mostpopular/\(versionURL)/\(popularKey)/\(30).json?api-key=\(Keys.apiKey)"
        self.overlayView.showOverlay(view: self.view)
        DataManager.sharedInstance().ArticleList(apiName: urlString,closure: { (result) in
            print(result)
            switch result {
            case .success(let dataList):
                print(dataList)
                self.articleListArray = dataList
                self.overlayView.hideOverlayView()
                let main = UIStoryboard(name: "Main", bundle: Bundle.main)
                let searchArticleVC = main.instantiateViewController(withIdentifier: "articleListViewController") as! ArticleListViewController
                searchArticleVC.searchListArray = self.articleListArray;
                self.navigationController!.pushViewController(searchArticleVC, animated:true)
                
                break
                
            case .failure(let error):
                print(error)
                self.overlayView.hideOverlayView()
                self.showAlert(title: "Alert", message: "Server Not Responding")
                
            }
            
        })
        
    }
    
    
}

extension NYTViewController: UITableViewDataSource,UITableViewDelegate{
    
    // MARK: Tableview delegate and Datasource methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return headerNames.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionNames[section].count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return headerNames[section]
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var cell = nytTableView.dequeueReusableCell(withIdentifier: "nytCell", for: indexPath) as? NYTCell
        if cell == nil {
            cell = nytTableView.dequeueReusableCell(withIdentifier: "nytCell", for: indexPath) as? NYTCell
        }
        cell?.selectionStyle = .none
        cell?.accessoryType = .disclosureIndicator
        var name = sectionNames[indexPath.section][indexPath.row]
        if indexPath.section == 0 {
            cell?.nameLabel.text = name
        }else{
            name = "Most " + name
            cell?.nameLabel.text = name
        }
        
        self.nytTableView.separatorStyle = .none

        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let main = UIStoryboard(name: "Main", bundle: Bundle.main)
            let searchArticleVC = main.instantiateViewController(withIdentifier: "searchArticleViewController") as! SearchArticleViewController
            self.navigationController!.pushViewController(searchArticleVC, animated:true)
        } else {
            if Connectivity.isConnectedToInternet {
                popularKeyData(popularKey: sectionNames[1][indexPath.row])
            }
        }
    }
    
}
