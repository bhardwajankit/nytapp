//
//  NYTViewController.swift
//  NYTApp
//
//  Created by Ankit on 9/6/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import UIKit
import SwiftyJSON


class DataManager {
    
    // MARK: Created Shared instance of DataManager class
    class func sharedInstance() -> DataManager {
        struct Static {
            static let sharedInstance = DataManager()
        }
        return Static.sharedInstance
    }
    
    // MARK: ArticleSearchList Request API call
    func SearchList(apiName: String, closure: @escaping (Result<[SearchList],Error>) -> Void) {
        
        ServerManager.sharedInstance().getRequest(apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                let userList = JSONMapper.searchList(responseJson: response["response"]["docs"] as JSON)
                closure(.success(userList))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
    
    // MARK: ArticleList Request API call
    func ArticleList(apiName: String, closure: @escaping (Result<[SearchList],Error>) -> Void) {
        
        ServerManager.sharedInstance().getRequest(apiName: apiName, extraHeader: nil) { (result) in
            switch result {
            case .success(let response):
                let userList = JSONMapper.articleList(responseJson: response["results"] as JSON)
                closure(.success(userList))
            case .failure(let error):
                closure(.failure(error))
            }
        }
    }
}

