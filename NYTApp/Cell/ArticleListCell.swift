//
//  ArticleListCell.swift
//  NYTApp
//
//  Created by Ankit  Bhardwaj on 06/09/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import UIKit

class ArticleListCell: UITableViewCell {
    @IBOutlet weak var articleView: UIView!
    @IBOutlet weak var headLineLabel: UILabel!
    @IBOutlet weak var publishDateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // uiview Border
        articleView.addShadow()

        // Initialization code
    }
    
    var articleData = SearchList() {
        didSet{
            bindData()
        }
    }
    
    func bindData()  {
        self.headLineLabel.text = articleData.nytHeadLine
        self.publishDateLabel.text = articleData.pubDate
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
