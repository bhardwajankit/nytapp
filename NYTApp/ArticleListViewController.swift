//
//  ArticleListViewController.swift
//  NYTApp
//
//  Created by Ankit on 9/6/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import UIKit

class ArticleListViewController: UIViewController {

    @IBOutlet weak var articleTableView: UITableView!
    var searchListArray = [SearchList]()
    var popularKey = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Article"
        // Do any additional setup after loading the view.
    }
    
}

extension ArticleListViewController: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchListArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 78
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = articleTableView.dequeueReusableCell(withIdentifier: "articleListCell", for: indexPath) as? ArticleListCell
        if cell == nil {
            cell = articleTableView.dequeueReusableCell(withIdentifier: "articleListCell", for: indexPath) as? ArticleListCell
        }
        cell?.selectionStyle = .none
        cell?.articleData = searchListArray[indexPath.row]
//        let name = sectionNames[indexPath.section][indexPath.row]
//        cell?.nameLabel.text = name
        
        return cell!
    }
    
    
}
