//
//  NYTCell.swift
//  NYTApp
//
//  Created by Ankit on 9/6/19.
//  Copyright © 2019 Ankit. All rights reserved.
//

import UIKit
import SwiftyJSON

class JSONMapper: NSObject {
    
    class func searchList(responseJson: JSON) -> [SearchList] {
        
        var searchListDetail = [SearchList]()
        for searchListString in responseJson {
            let searchList = SearchList()
            
            if let headLine = searchListString.1["headline"]["main"].string {
                searchList.nytHeadLine = headLine
            }
            if let publishDate = searchListString.1["pub_date"].string {
                searchList.pubDate = publishDate
            }
            
            searchListDetail.append(searchList)
            
        }
        
        return searchListDetail
    }
    
    class func articleList(responseJson: JSON) -> [SearchList] {
        
        var articleListDetail = [SearchList]()
        for article in responseJson {
            let articleList = SearchList()
            
            if let headLine = article.1["title"].string {
                articleList.nytHeadLine = headLine
            }
            if let publishDate = article.1["published_date"].string {
                articleList.pubDate = publishDate
            }
            
            articleListDetail.append(articleList)
            
        }
        
        return articleListDetail
    }
}

